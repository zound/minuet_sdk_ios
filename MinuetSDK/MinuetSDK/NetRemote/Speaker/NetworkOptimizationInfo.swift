//
//  NetworkOptimizationInfo.swift
//  MinuetSDK
//
//  Created by Dolewski Bartosz on 19.01.2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import Foundation

public struct NetworkOptimizationInfo {
    public let speaker: Speaker
    public let optimization: Bool
}
