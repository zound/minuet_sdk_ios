//
//  SpeakerPresets.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 24/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

public enum SpeakerPresetsError: Error {
    
    case spotifyNotAuthenticated
    case presetHasNoType
}

public class SpeakerPresets {
    
    let speaker: Speaker
    let speakerNotifier: SpeakerNotifier
    let provider: SpeakerProvider
    let spotifyProvider: SpotifyProvider
    
    
    public init(speaker: Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider, spotifyProvider: SpotifyProvider) {
        
        self.speaker = speaker
        self.speakerNotifier = speakerNotifier
        self.provider = provider
        self.spotifyProvider = spotifyProvider
    }
    
    public func getPresets() -> Observable<[Preset]> {
        
        return SpeakerPresets.getPresetsForSpeaker(speaker, provider: provider)
    }
    
    public static func getPresetsForSpeaker(_ speaker: Speaker, provider: SpeakerProvider) -> Observable<[Preset]> {
        
        let getPresets: Observable<[Preset]> = provider
            .setNode(ScalarNode.NavState, value: "1", forSpeaker: speaker)
            .flatMap{ (didSetNode: Bool) -> Observable<[Preset]> in
                
                return provider.getListNode(ListNode.Presets,maxCount:7, forSpeaker: speaker)
        }
        
        return getPresets
    }
    
    public static func uploadPresets(_ presets: [Preset], toSpeaker speaker: Speaker, provider: SpeakerProvider) -> Observable<Bool> {
        
        var uploadPresets = provider.setNode(ScalarNode.NavState, value: "0", forSpeaker: speaker).flatMapLatest{ done in
            provider.setNode(ScalarNode.NavState, value: "1", forSpeaker: speaker)
        }
        for preset in presets {
            uploadPresets = uploadPresets.flatMapLatest{ done -> Observable<Bool> in
                return SpeakerPresets.uploadPreset(preset, toSpeaker: speaker, provider: provider).flatMapLatest{ done in
                        return Observable.just(done)
                    }
                    .catchErrorJustReturn(false)
            }
        }
        
        return uploadPresets
    }
    
    public static func uploadPreset(_ preset: Preset, toSpeaker speaker: Speaker, provider: SpeakerProvider) -> Observable<Bool> {
        
        if let blob = preset.blob {
            
            let presetUploadNodes: [ScalarNode: String]  =
                [
                    ScalarNode.NavPresetDelete: String(preset.number-1),
                    ScalarNode.NavPresetUploadType: preset.type!.modeIdentifier(),
                    ScalarNode.NavPresetUploadName: preset.name,
                    ScalarNode.NavPresetUploadArtworkURL: preset.imageURL?.absoluteString ?? "",
                    ScalarNode.NavPresetUploadBlob: blob]
            NSLog("ZOUND_1259 Uploading preset \(presetUploadNodes) with blob field: \(blob)")
            
            return provider.setNodes(presetUploadNodes, forSpeaker: speaker)
                        .flatMapLatest{ nodeSuccesses -> Observable<[ScalarNode: Bool]>  in
                            
                            return provider.setNode(ScalarNode.NavPresetUploadUpload, value: String(preset.number-1), forSpeaker: speaker).map{ success in
                                
                                var nodeSuccessesCopy = nodeSuccesses
                                nodeSuccessesCopy[ScalarNode.NavPresetUploadUpload] = success
                                return nodeSuccessesCopy }
                            
                        }.map{ nodesSuccesses in
                            
                            var allSuccessful = true
                            for (_, value) in nodesSuccesses {
                                
                                if value == false {
                                    allSuccessful = false
                                    break;
                                }
                            }
                            return allSuccessful
                    }
                    
            }
        
        
        return Observable.just(false)
    }
    
}
