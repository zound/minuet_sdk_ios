//
//  vTunerCrypt.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation


import IDZSwiftCommonCrypto

func vTunerEncryptMac(_ mac: String, withEncryptedToken encryptedToken:String) -> String? {
    
    let key : [UInt8] = [ 0x0f, 0x2f, 0xbe, 0x6c, 0x1e, 0x6b, 0xc3, 0x13, 0x83, 0x69, 0xc9, 0x5d, 0xe0, 0x9b, 0xf9, 0xc5]
    let iv: [UInt8] = [0xa3,0xab,0x73,0x29,0xb9,0x61,0x82,0x53]
    
    let tokenDecryptor = Cryptor(operation:.Decrypt, algorithm:.Blowfish, options:.PKCS7Padding, key:key, iv:iv)
    if let decryptedTokenChars = tokenDecryptor.update(arrayFromHexString(encryptedToken))?.final() {
        
        let paddedDecryptedToken = hexStringFromArray(decryptedTokenChars)
        let first12Range = paddedDecryptedToken.startIndex ..< paddedDecryptedToken.startIndex.advancedBy(12)
        let decryptedToken = paddedDecryptedToken[first12Range]
        
        let stringToEncrypt = mac + decryptedToken + "00000000"
        let stringToEncryptChars = arrayFromHexString(stringToEncrypt)
        let tokenEncryptor = Cryptor(operation:.Encrypt, algorithm:.Blowfish, options:.PKCS7Padding, key:key, iv:iv)
        
        if let encryptedTokenChars = tokenEncryptor.update(stringToEncryptChars)?.final() {
            
            let encryptedMacComplete = hexStringFromArray(encryptedTokenChars)
            let first32Range = encryptedMacComplete.startIndex ..< encryptedMacComplete.startIndex.advancedBy(32)
            let encryptedMac = encryptedMacComplete[first32Range]
            
            return encryptedMac
        }
    }
    return nil
}
