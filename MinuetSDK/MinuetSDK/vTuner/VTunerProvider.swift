//
//  VTunerProvider.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import SWXMLHash

class VTunerProvider:RxMoyaProvider<VTunerService> {
    
    override init(endpointClosure: EndpointClosure = MoyaProvider.DefaultEndpointMapping,
                  requestClosure: RequestClosure = MoyaProvider.DefaultRequestMapping,
                  stubClosure: StubClosure = MoyaProvider.NeverStub,
                  manager: Manager = VTunerProvider.VTunerManager(),
                  plugins: [PluginType] = []) {
        super.init(endpointClosure: endpointClosure, requestClosure: requestClosure, stubClosure: stubClosure, manager: manager, plugins: plugins)
    }
    
    func encryptedMacForSpeaker(_ speaker: Speaker) -> Observable<String> {
        
        return request(VTunerService.GetEncryptedToken).mapVTunerResponse().map{ (encToken: EncryptedToken) in
            encToken.value
        }
    }
    
    func rootMenuForSpeaker(_ speaker: Speaker, encryptedToken: String) -> Observable<[VTunerListItem]> {
        
        return request(VTunerService.GetRoot(speaker: speaker, encryptedToken: encryptedToken)).mapVTunerResponse().map{ (listResponse: VTunerListResponse<VTunerListItem>) in
        
            return listResponse.items ?? []
        }
    }
    
    func itemsInFolder(_ folderName: String, fromIndex: Int, maxCount: Int) -> Observable<[VTunerListItem]> {
        
        return request(VTunerService.GetFolder(folderName: folderName, fromIndex: fromIndex, maxCount: maxCount)).mapVTunerResponse().map{ (listResponse: VTunerListResponse<VTunerListItem>) in
            
            return listResponse.items ?? []
        }
        
    }
}

extension VTunerProvider {
    
   static func VTunerManager() -> Manager {
        let configuration = URLSessionConfiguration.default
        var headers:[String: String] = [:]
        headers["User-Agent"] = "FSL IR/0.1"
        headers["Cache-Control"] = "no-cache"
        headers["Pragma"] = "no-cache"
        configuration.httpAdditionalHeaders = headers
        
        let manager = Manager(configuration: configuration)
        manager.startRequestsImmediately = false
        return manager
    }
}
