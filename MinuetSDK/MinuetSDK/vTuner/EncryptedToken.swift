//
//  EncryptedToken.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import SWXMLHash

struct EncryptedToken {
    
    let value: String
}

extension EncryptedToken: XMLIndexerDeserializable {
    
    static func deserialize(_ node: XMLIndexer) throws -> EncryptedToken {
        
        return try EncryptedToken(
            value: node.value()
        )

    }
}
