//
//  VTunerListResponse.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import SWXMLHash

struct VTunerListResponse<T:XMLIndexerDeserializable>: XMLIndexerDeserializable {
    
    let itemCount: Int
    let items: [T]?
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerListResponse {
        return try VTunerListResponse(
            itemCount: node["ItemCount"].value(),
            items: try? node["Item"].value()
        )
    }
}
