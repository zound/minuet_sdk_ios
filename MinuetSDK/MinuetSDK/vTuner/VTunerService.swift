//
//  VTunerService.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import Alamofire

enum VTunerSearchType:Int {
    
    case podcastId = 1
    case radioId = 2
    case radioName = 3
    case podcastName = 5
}

enum VTunerService {
    
    case getEncryptedToken
    case getRoot(speaker: Speaker, encryptedToken: String)
    case getFolder(folderName: String, fromIndex: Int, maxCount: Int)
    case getStationById(stationId: String)
    case getStationsByName(stationName: String, fromIndex: Int, maxCount: Int)
}

extension VTunerService: TargetType {
    
    var baseURL: URL {
        switch self {
        case .getEncryptedToken,.getRoot(_):return URL(string:"http://pri.logon.wifiradiofrontier.com/setupapp/fs/asp/BrowseXML")!
        default: return URL(string:"http://pri.wifiradiofrontier.com/setupapp/fs/asp/BrowseXML")!
        }
    }
    
    var path: String {
        switch self {
        case .getEncryptedToken, .getRoot(_): return "/loginXML.asp"
        case .getFolder(_): return "/navXML.asp"
        case .getStationById(_), .getStationsByName(_) : return "/Search.asp"
        }
    }
    
    var method: Moya.Method {
        
        return .GET
    }
    
    var parameters: [String: AnyObject]? {
        switch self {
        case .getEncryptedToken:
            return ["token":0 as AnyObject]
        case .getRoot(let speaker, let encryptedToken):
            return ["gofile":"" as AnyObject,
                    "mac" : vTunerEncryptMac(speaker.mac, withEncryptedToken: encryptedToken) ?? "",
                    "dlang":"eng",
                    "fver":6]
        case .getFolder(let folderName, let fromIndex, let maxCount):
            return ["gofile":folderName as AnyObject,
                    "startItems": (fromIndex + 1),
                    "endItems": (fromIndex + maxCount),
                    "dlang":"eng",
                    "fver":6]
        case .getStationById(let stationId):
            return ["sSearchType":VTunerSearchType.radioName.rawValue as AnyObject,
                    "Search":stationId as AnyObject,
                    "startItems": 1 as AnyObject,
                    "endItems": 1 as AnyObject,
                    "dlang":"eng" as AnyObject,
                    "fver":6 as AnyObject]
        case .getStationsByName(let stationName, let fromIndex, let maxCount):
            return ["sSearchType":VTunerSearchType.radioName.rawValue as AnyObject,
                    "Search":stationName as AnyObject,
                    "startItems": fromIndex+1,
                    "endItems": fromIndex + maxCount ,
                    "dlang":"eng",
                    "fver":6]
        }
        
    }
    
    var sampleData: Data {
     
        return Data()
    }
}
