//
//  AirplaySourceViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

protocol AirplaySourceViewControllerDelegate: class {
    
    func airplaySourceDidRequestBack(_ viewController: AirplaySourceViewController)
    func airplaySourceDidRequestOpenApp(_ viewController: AirplaySourceViewController)
}


class AirplaySourceViewController: UIViewController {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var openAppleMusicButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentLabel.text = Localizations.AirplaySource.Content
        contentLabel.font = Fonts.UrbanEars.Regular(16)
        
        UIView.setAnimationsEnabled(false)

        
        backButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        openAppleMusicButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.AirplaySource.OpenAirplay), for: .normal)
        
        backButton.layoutIfNeeded()
        openAppleMusicButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onAppleMusic(_ sender: AnyObject) {
     
        self.delegate?.airplaySourceDidRequestOpenApp(self)
    }
    
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.airplaySourceDidRequestBack(self)
    }
    weak var delegate: AirplaySourceViewControllerDelegate?
}
