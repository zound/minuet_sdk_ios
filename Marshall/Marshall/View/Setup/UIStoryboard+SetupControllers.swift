//
//  UIStoryboard+SetupControllers.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 18/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit


extension UIStoryboard {
    static var setup: UIStoryboard {
        return UIStoryboard(name: "Setup", bundle: nil)
    }
}

extension UIStoryboard {
    
    var setupViewController: SetupViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setup") as? SetupViewController else {
            fatalError("SetupViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupListViewController: SetupListViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "speakerList") as? SetupListViewController else {
            fatalError("SetupListViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupLoadingViewController: SetupLoadingViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupLoading") as? SetupLoadingViewController else {
            fatalError("SetupLoadingViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupFailedViewController: SetupFailedViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupFailed") as? SetupFailedViewController else {
            fatalError("SetupFailedViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupFailedConnectViewController: SetupFailedConnectViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupFailedConnect") as? SetupFailedConnectViewController else {
            fatalError("SetupFailedConnectViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupFailedBootViewController: SetupFailedBootViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupFailedBoot") as? SetupFailedBootViewController else {
            fatalError("SetupFailedBootViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupFailedSetupViewController: SetupFailedSetupViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupFailedSetup") as? SetupFailedSetupViewController else {
            fatalError("SetupFailedSetupViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupFailedResetViewController: SetupFailedResetViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupFailedReset") as? SetupFailedResetViewController else {
            fatalError("SetupFailedResetViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsIntroViewController: SetupPresetsIntroViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupPresetsIntro") as? SetupPresetsIntroViewController else {
            fatalError("SetupPresetsIntroViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsPickerViewController: SetupPresetsPickerViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "presetsPicker") as? SetupPresetsPickerViewController else {
            fatalError("SetupPresetsPickerViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsLoadingViewController: SetupPresetsLoadingViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "presetsLoading") as? SetupPresetsLoadingViewController else {
            fatalError("SetupPresetsLoadingViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsLoadingProgressViewController: SetupPresetsLoadingProgress{
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "presetsLoadingProgress") as? SetupPresetsLoadingProgress else {
            fatalError("SetupPresetsLoadingProgres couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsLoadingErrorViewController: SetupPresetsLoadingError {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "presetsLoadingError") as? SetupPresetsLoadingError else {
            fatalError("SetupPresetsLoadingError couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsLoadingPreparingViewController: SetupPresetsLoadingPreparing {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "presetsLoadingPreparing") as? SetupPresetsLoadingPreparing else {
            fatalError("SetupPresetsLoadingPreparing couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsFinalViewController: SetupPresetsFinalViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "presetsFinal") as? SetupPresetsFinalViewController else {
            fatalError("SetupPresetsFinalViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsTutorialViewController: SetupPresetsTutorialViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "presetsTutorial") as? SetupPresetsTutorialViewController else {
            fatalError("SetupPresetsTutorialViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupTutorialViewController: SetupTutorialViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupTutorial") as? SetupTutorialViewController else {
            fatalError("SetupTutorialViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupTutorialCloudViewController: SetupTutorialCloudViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupTutorialCloud") as? SetupTutorialCloudViewController else {
            fatalError("SetupTutorialCloudViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupTutorialRadioViewController: SetupTutorialRadioViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupTutorialRadio") as? SetupTutorialRadioViewController else {
            fatalError("SetupTutorialRadioViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupTutorialMultiViewController: SetupTutorialMultiViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupTutorialMulti") as? SetupTutorialMultiViewController else {
            fatalError("SetupTutorialMultiViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupTutorialCastViewController: SetupTutorialCastViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupTutorialCast") as? SetupTutorialCastViewController else {
            fatalError("SetupTutorialCastViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsSpotifyViewController: SetupPresetsSpotifyViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "connectSpotify") as? SetupPresetsSpotifyViewController else {
            fatalError("SetupPresetsSpotifyViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupPresetsSpotifySuccessViewController: SetupPresetsSpotifySuccessViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "spotifySuccess") as? SetupPresetsSpotifySuccessViewController else {
            fatalError("SetupPresetsSpotifySuccessViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var setupDoneViewController: SetupDoneViewController {
        guard let vc = UIStoryboard.setup.instantiateViewController(withIdentifier: "setupDone") as? SetupDoneViewController else {
            fatalError("SetupDoneViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
}
