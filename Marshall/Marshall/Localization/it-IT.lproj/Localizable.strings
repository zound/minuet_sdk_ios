/* Localizable.strings
  UrbanEars

  Created by Raul Andrisan on 11/07/16.
  Copyright © 2016 Zound Industries. All rights reserved. */

"appwide.note" = "NOTA";

"appwide.continue" = "CONTINUA";

"appwide.done" = "FATTO";

"appwide.back" = "INDIETRO";

"appwide.next" = "AVANTI";

"appwide.cancel" = "ANNULLA";

"appwide.skip" = "SALTA";

"appwide.go_to_website" = "VAI AL SITO";

"about.eula.content" = "La versione completa del Contratto di licenza per l’utente finale può essere visualizzata sul sito www.marshallheadphones.com.";

"about.eula.title" = "Contratto di licenza per l’utente finale";

"about.foss.content" = "La versione completa del software libero e open source può essere visualizzata sul sito www.marshallheadphones.com.";

"about.foss.title" = "FOSS";

"about.privacy_policy.title" = "politica sulla privacy";

"about.menu_item.eula" = "Contratto di licenza per l’utente finale";

"about.menu_item.foss" = "Software libero e open source";

"about.title" = "INFORMAZIONI";

"about.version" = "Versione Marshall Multi-Room\n %@ (%@)";

"airplay_source.content" = "Con AirPlay è possibile riprodurre qualsiasi fonte musicale direttamente dall’altoparlante tramite il Centro di Controllo del tuo dispositivo iOS.";

"browse_radio.search_placeholder" = "Ricerca di una stazione radio";

"browse_radio.title" = "SFOGLIA RADIO";

"cast_info.buttons.get_google_cast" = "OTTIENI L’APP GOOGLE HOME";

"cast_info.buttons.learn_more" = "MAGGIORI INFORMAZIONI";

"cast_info.content" = "Questo altoparlante è in riproduzione tramite Chromecast integrato.\n\nUtilizzare l’app Google Home per costituire gruppi di Cast Multi-room.\n\nI tuoi gruppi multi-room appariranno sotto il pulsante Cast nella tua app musicale.";

"CFBundleDisplayName" = "Multi-Room";

"CFBundleName" = "Multi-Room";

"google_cast_source.content" = "Il tuo altoparlante è disponibile sotto l’icona Cast in varie app.";

"google_cast_source.get_cast_apps" = "TROVA APP ABILITATE AL CAST";

"google_cast_source.note" = "NOTA\n Per riprodurre musica dagli altoparlanti attraverso Chromecast integrato, devi configurare un gruppo multiplo nella app Google Home.";

"group_full.content" = "Stai tentando di aggiungere un sesto altoparlante in modalità multipla. Ci fa piacere che abbia acquistato tanti altoparlanti, ma al momento il sistema di altoparlanti Marshall Multi-Room supporta solo la modalità multipla con cinque altoparlanti nella stessa rete Wi-Fi.";

"help.contact.button.send_email" = "CONTATTO ASSISTENZA";

"help.contact.content" = "Visita il nostro sito www.marshallheadphones.com \no contatta l’assistenza.";

"help.contact.title" = "CONTATTO";

"help.menu_item.contact" = "Contatto";

"help.menu_item.online_manual" = "Manuale online";

"help.menu_item.quick_guide" = "Guida rapida";

"help.online_manual_content" = "I manuali per l’utente per gli altoparlanti Marshall Speakers sono disponibili in varie lingue sul sito www.marshallheadphones.com.";

"help.online_manual.title" = "MANUALE ONLINE";

"help.quick_guide.menu_item.presets" = "Preset";

"help.quick_guide.menu_item.solo_multi" = "Modalità singola/Modalità multipla";

"help.quick_guide.menu_item.speaker_knobs" = "Controllo altoparlante";

"help.quick_guide.title" = "GUIDA RAPIDA";

"help.title" = "AIUTO";

"home.speaker.playing_source" = "%@";

"home.speaker.switch_label.multi" = "MULTI";

"home.speaker.switch_label.solo" = "SINGLE";

"loading.loading_speakers" = "Ricerca di altoparlanti connessi in corso…";

"menu.about" = "Informazioni";

"menu.add_speaker" = "Configurazione";

"menu.help" = "Aiuto";

"menu.shop" = "Negozio";

"menu.speaker_settings" = "Impostazioni";

"no_speakers.buttons.refresh" = "AGGIORNA";

"no_speakers.buttons.set_up_speakers" = "CONFIGURAZIONE";

"no_speakers.description" = "Nessun altoparlante installato su questa rete Wi-Fi.";

"no_wifi.buttons.go_to_settings" = "VAI ALLE IMPOSTAZIONI";

"no_wifi.description" = "Nessuna connessione Wi-Fi disponibile sul tuo dispositivo. Verificare la connessione e riprovare.";

"no_wifi.title" = "SPIACENTE, NESSUN SEGNALE";

"player.aux.activate" = "ATTIVA";

"player.aux.activated" = "ATTIVATA";

"player.aux.audio_active" = "Audio attivo";

"player.bluetooth.activate" = "ATTIVA";

"player.bluetooth.activated" = "ATTIVATA";

"player.bluetooth.connected" = "Bluetooth - Connesso";

"player.bluetooth.connected_to" = "Connesso a";

"player.bluetooth.enter_pairing" = "MODALITÀ ABBINAMENTO";

"player.bluetooth.idle" = "Inattivo";

"player.bluetooth.waiting_to_pair" = "ATTENDERE L’ABBINAMENTO";

"player.cloud.airplay" = "AIRPLAY";

"player.cloud.google_cast" = "CHROMECAST INTEGRATO";

"player.cloud.or_play_mobile_app" = "Riproduci dall’app mobile";

"player.cloud.play_internet_radio" = "RADIO SU INTERNET";

"player.cloud.spotify_connect" = "SPOTIFY";

"player.connecting.reconnecting" = "Riconnessione in corso…";

"player.now_playing.browse_stations" = "SFOGLIA RADIO";

"player.now_playing.save_preset" = "AGGIUNGI PRESET";

"player.preset.empty_placeholder" = "Preset vuoto";

"player.preset.notification.add" = "Preset aggiunto";

"player.preset.notification.add_playlist" = "Playlist aggiunta";

"player.preset.notification.add_radio_station" = "Stazione radio aggiunta";

"player.preset.notification.addfailed" = "Aggiunta preset non riuscita";

"player.preset.notification.addfailedinternetradio" = "Aggiunta stazione radio non riuscita";

"player.preset.notification.addfailedplaylist" = "Aggiunta playlist non riuscita";

"player.preset.notification.delete" = "Preset cancellato";

"player.preset.notification.delete_internet_radio" = "Stazione radio cancellata";

"player.preset.notification.delete_playlist" = "Playlist cancellata";

"player.preset.notification.empty" = "Il preset è vuoto";

"player.preset.notification.play" = "Riproduzione preset";

"player.preset.spotify_login_error.content" = "Impossibile connettere l’altoparlante\nal tuo account Spotify. \n\nRiprova o torna indietro.";

"player.preset.spotify_login_error.retry_button" = "RIPROVA";

"player.preset.spotify_login_error.title" = "QUALCOSA È ANDATO STORTO";

"player.preset.spotify_premium_error.content" = "Il tuo account non è Premium. Per riprodurre Spotify dal tuo altoparlante devi avere un account Premium.";

"player.preset.spotify_premium_error.dismiss_button" = "OK";

"player.preset.spotify_premium_error.title" = "Errore Spotify";

"player.session_lost.buttons.reconnect" = "RICONNETTI";

"player.session_lost.generic_error_to_speaker" = "Si è verificato un errore sconosciuto durante la comunicazione con %@. Prova a riconnetterti premendo \"Riconnetti\"";

"player.session_lost.network_connection_lost_to_speaker" = "%@ ha perso la connessione Wi-Fi. Premi \"Riconnetti\" e torna a ballare.";

"player.session_lost.speaker_not_connected" = "L’altoparlante non è ancora connesso";

"player.session_lost.timeout" = "Il tempo per la connessione a %@ è scaduto. Premi \"Riconnetti\" per riprovare.";

"player.session_lost.user_is_controlling_speaker" = "%@ è sotto il controllo di un altro utente. Pronto a prendere il controllo? \nPremi \"Riconnetti\" per prendere controllo dell’altoparlante.";

"presets_disabled.buttons.read_more" = "MAGGIORI INFORMAZIONI";

"presets_disabled.content" = "Per aggiungere un preset è necessario riprodurre:\n\n- Musica tramite Spotify Connect\n- Radio online (che si trova sotto [cloud_image])\n\nQuando una di queste fonti è in riproduzione, il pulsante di aggiunta ( [plus_image] ) sarà abilitato e toccandolo si aggiungerà la playlist/stazione radio in riproduzione al momento.";

"presets_disabled.title" = "COME AGGIUNGERE UN PRESET";

"save_preset.title" = "AGGIUNGI PRESET";

"settings.about.cast_version" = "Versione Chromecast integrato:";

"settings.about.ip" = "IP:";

"settings.about.name" = "Nome:";

"settings.about.mac" = "MAC:";

"settings.about.model" = "Modello:";

"settings.about.title" = "INFORMAZIONI SU QUESTO ALTOPARLANTE";

"settings.about.wi_fi_network" = "Rete Wi-Fi:";

"settings.list.title" = "ALTOPARLANTI SINGOLI";

"settings.speaker.about" = "Informazioni su questo altoparlante";

"settings.speaker.rename" = "Assegna un nuovo nome all’altoparlante";

"settings.speaker.streaming_quality" = "Qualità Multi streaming";

"settings.streaming_quality.cellLabel" = "Qualità Multi streaming";

"settings.streaming_quality.description" = "L’impostazione Qualità normale regola la qualità in modo dinamico in base alle condizioni della rete ed è ideale in presenza di interruzioni. L’impostazione Qualità alta sfrutta sempre la massima qualità.\n\nLe impostazioni di qualità per lo streaming multiplo riguardano esclusivamente la modalità multipla.";

"settings.streaming_quality.high" = "Qualità alta";

"settings.streaming_quality.normal_recommended" = "Qualità normale (opzione consigliata)";

"settings.streaming_quality.title" = "QUALITÀ MULTI STREAMING";

"setup.connect_spotify.buttons.connect_to_spotify" = "CONNETTI A SPOTIFY";

"setup.connect_spotify.paragraph1.content" = "È facile riprodurre Spotify dal \ntuo altoparlante.\n\nUna volta connesso, apri l’app Spotify per riprodurre e gestire la musica senza fili.";

"setup.connect_spotify.paragraph1.title" = "Connetti a Spotify";

"setup.done.content" = "Sei pronto per goderti il sistema senza fili Marshall Multi-Room.";

"setup.done.title" = "Completato";

"setup.failed_boot.message" = "Quando entrambi i LED sono accesi, \nl’altoparlante è in fase di avvio. Questa fase richiederà circa 20 secondi.";

"setup.failed_connect.message" = "Assicurati che l’altoparlante sia collegato alla rete elettrica.";

"setup.failed_reset.message" = "Ci sono ancora problemi? Consulta la guida rapida nella sezione “Aiuto” per maggiori informazioni sulla configurazione.";

"setup.failed_setup.buttons.no" = "NO";

"setup.failed_setup.buttons.yes" = "SÌ";

"setup.failed_setup.message" = "Quando tutti i LED sulla \nmanopola Source lampeggiano, \nl’altoparlante è in modalità installazione.\n\nI LED lampeggiano?";

"setup.list.found_speakers_with_count" = "Tutto ok. %d altoparlante/i trovato/i. \nSelezionane uno per proseguire.";

"setup.loading.looking_for_speakers" = "Ricerca di altoparlanti connessi in corso…";

"setup.pick_presets.content" = "Connettiti a Spotify e alla radio online per impostare i preset.";

"setup.pick_presets.internet_radio_description" = "Oltre 30.000 stazioni da tutto il mondo";

"setup.pick_presets.internet_radio_title" = "Radio online";

"setup.pick_presets.spotify_description" = "Playlist, album, artisti, generi e podcast";

"setup.pick_presets.title" = "Aggiunta preset";

"setup.presets_fail.content" = "Sembra che ci sia stato qualche problema nell’aggiunta dei preset all’altoparlante. Riprova o torna indietro. È sempre possibile aggiungere preset in seguito.";

"setup.presets_list.title" = "I tuoi preset";

"setup.presets_loading.adding_preset_title" = "Aggiunta %@";

"setup.presets_loading.done" = "Fatto";

"setup.presets_loading.title" = "Aggiunta preset";

"setup.presets_tutorial.content" = "Per riprodurre un preset, ruota la manopola su un numero e premi.\n\nPer salvare un preset, ruota la manopola su un numero e tieni premuto mentre ascolti musica su Spotify o sulla radio online.";

"setup.presets_tutorial.title" = "Utilizzo preset";

"setup.spotify_success.title" = "Tutto fatto";

"setup.spotify_success.your_spotify_account" = "Accesso a Spotify effettuato";

"setup.tutorial.cast.content_top" = "Chromecast integrato ti consente di riprodurre sui tuoi altoparlanti musica, radio online o podcast dalle tue app musicali preferite.";

"setup.tutorial.cast.title" = "Chromecast integrato";

"setup.tutorial.cloud.airplay" = "AirPlay";

"setup.tutorial.cloud.content" = "Cerca uno di questi simboli \nnella tua app musicale preferita. \nClicca il simbolo quando appare e \nseleziona l’altoparlante. Sarai pronto \nin un batter d’occhio.";

"setup.tutorial.cloud.google_cast" = "Chromecast integrato";

"setup.tutorial.cloud.spotify_connect" = "Spotify Connect";

"setup.tutorial.cloud.title" = "Wi-Fi";

"setup.tutorial.internet_radio.content" = "Sfoglia e riproduci oltre 30.000 stazioni radio online da questa app.\nLe stazioni radio online si trovano nella\ncategoria Wi-Fi dell’app.";

"setup.tutorial.internet_radio.title" = "Radio online";

"setup.tutorial.multi.content" = "È possibile raggruppare più altoparlanti Multi-Room per un playdate sincronizzato. Imposta un altoparlante in modalità multipla tramite questa app o premendo la manopola Multipla/Singola sul pannello superiore degli altoparlanti.";

"spotify_source.buttons.get_spotify" = "OTTIENI SPOTIFY";

"spotify_source.buttons.open_spotify" = "APRI SPOTIFY";

"spotify_source.instructions" = "1. \tApri l’app Spotify su smartphone, tablet o laptop.\n2. \tRiproduci un brano e seleziona Dispositivi disponibili.\n3.\tSeleziona il dispositivo e inizia ad ascoltare.";

"spotify_source.intro" = "Ti va di ascoltare un po’ di musica?\nAscolta Spotify su %@, utilizzando l’app Spotify come telecomando.";

"volume.headers.master_volume_label" = "Volume multiplo";

"volume.title" = "VOLUME";

"welcome.title" = "BENVENUTO";

"about.eula.website" = "https://www.marshallheadphones.com/multi-room-speaker-eula-it";

"about.privacy_policy.website" = "https://www.marshallheadphones.com/app-privacy-policy-it";

"welcome.buttons.accept" = "INIZIO";

"setup.presets_fail.title" = "PRESET NON SALVATO";

"setup.presets_fail.buttons.try_again" = "RIPROVA";

"help.contact.website" = "https://www.marshallheadphones.com";

"help.online_manual.website" = "https://www.marshallheadphones.com/multi-room-speaker-support";

"about.foss.website" = "https://www.marshallheadphones.com/multi-room-speaker-foss";

"volume.role.multi" = "MULTI";

"volume.role.solo" = "SINGLE";

"player.aux.mode_name" = "AUX";

"player.bluetooth.connect_device_advice" = "Verifica che il Bluetooth sul tuo %@ sia attivato.";

"setup.presets_loading.preparing_presets" = "Recupero preset";

"player.preset.login_spotify_advice" = "Clicca qui per connetterti a Spotify e aggiornare la grafica del preset.";

"player.cloud.internet_radio_mode_name" = "Radio online";

"player.cloud.google_cast_mode_name" = "Chromecast integrato";

"player.preset.spotify_playlist" = "Playlist Spotify";

"player.carousel.playable_source.aux" = "AUX";

"player.carousel.playable_source.preset" = "Preset";

"player.carousel.playable_source.bluetooth" = "Bluetooth";

"player.carousel.playable_source.cloud" = "Wi-Fi";

"welcome.subtitle" = "Grazie a competenze sviluppate in oltre mezzo secolo, il sistema senza fili Marshall ti fa vivere direttamente a casa tua un’esperienza live.";

"help.contact.support_website" = "https://www.marshallheadphones.com/multi-room-speaker-contact";

"no_speakers.title" = "NESSUN ALTOPARLANTE RILEVATO";

"player.rca.mode_name" = "RCA";

"player.carousel.playable_source.rca" = "RCA";

"player.rca.activate" = "ATTIVA";

"player.rca.activated" = "ATTIVATA";

"player.rca.audio_active" = "Audio attivo";

"volume.volume_slider_label" = "Volume";

"volume.bass_slider_label" = "Bassi";

"volume.treble_slider_label" = "Alti";

"volume.eq_button_show_text" = "EQ";

"volume.eq_button_hide_text" = "Nascondi";

"settings.speaker.led_intensity" = "Luce";

"setup.failed.title" = "GUIDA ALLA CONFIGURAZIONE";

"setup.pick_presets.content_bottom" = "Lasciaci aggiungere dei preset al tuo altoparlante \nper iniziare. È sempre possibile effettuare modifiche in seguito.";

"settings.led_adjuster.adjust_instruction" = "Regola l’intensità dei LED sul tuo altoparlante";

"settings.sounds.title" = "Suono";

"settings.sounds.bluetooth_connect" = "Bluetooth connesso";

"settings.sounds.network_connect" = "Connessione alla rete";

"settings.sounds.preset_store" = "Memorizzazione preset";

"settings.sounds.preset_fail" = "Impostazione preset non riuscita";

"reconnecting.reconnecting_to" = "Riconnessione a %@";

"settings.about.build_version" = "Versione firmware di sistema:";

"setup.list.multi_room" = "Multi-Room";

"setup.list.connected" = "Aggiungi preset";

"setup.list.connect_to_wifi" = "Connetti a Wi-Fi";

"setup.presets_initial.title" = "PRESET";

"setup.presets_initial.content" = "Sette preset ti danno accesso immediato alle tue stazioni radio online o artisti e playlist di Spotify preferiti.";

"setup.presets_initial.buttons.next" = "AGGIUNTA PRESET";

"settings.root_menu.title" = "IMPOSTAZIONI";

"settings.root_menu.solo_speakers" = "Altoparlanti singoli";

"settings.root_menu.multi_speakers" = "Altoparlanti multipli";

"settings.multi_menu.stereo_pairing" = "Abbinamento stereo";

"settings.multi_menu.multi_streaming_quality" = "Qualità Multi streaming";

"player.session_lost.user_is_controlling_speaker_title" = "CONTROLLO DI UN ALTRO UTENTE";

"player.session_lost.network_connection_lost_to_speaker_title" = "CONNESSIONE WI-FI ASSENTE";

"cast_switch.content" = "Spostare questo altoparlante in modalità Multi farà terminare questa sessione di Chromecast integrato.\n\nUtilizza l’app Google Home per costituire gruppi di Cast Multi-room.";

"unlocked_module.content" = "%@ is a prototype unit and will therefore stop working by the end of 2017. \n\nPlease get in touch with your point of contact at Zound Industries.";

"airplay_source.open_airplay" = "APRI APPLE MUSIC";

"setup.tutorial.multi.title" = "MODALITÀ MULTIPLA";

"volume.buttons.mute_all" = "DISATTIVA";

"volume.buttons.unmute_all" = "RIATTIVA";

"updateavailable_module.content" = "%@ non sta utilizzando l’ultimo aggiornamento per i diffusori Multi-Room. \n\nEffettua manualmente l’aggiornamento, accedendo alle Impostazioni del diffusore e selezionando \“Informazioni su questo diffusore\”. In alternativa, si aggiornerà automaticamente nel corso della notte.";

"settings.about.update.content" = "%@ è stato selezionato per essere aggiornato. \n\nDurante l’aggiornamento, il diffusore Multi-Room sarà provvisoriamente non disponibile. Ad aggiornamento ultimato, tornerà a comparire sulla schermata iniziale.";

"settings.about.update.buttonLabel" = "AGGIORNA IL DIFFUSORE";
