//
//  SettingsConfig.swift
//  UrbanEars
//
//  Created by Dolewski Bartosz A (Ext) on 21.05.2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

/// Characters limit allowed by specification while renaming speaker
let maximumCharactersInSpeakerName: Int = 32

