//
//  AboutState+Color.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

extension AboutState {
    
    public var modelName: String? {
        
        if let color = color,
            let displayModelName = ExternalConfig.sharedInstance.speakerImageForColor(color)?.displayModel {
            return displayModelName
        }
        
        return ""
    }
    
    public var colorName: String? {
        
        if let displayColorName = ExternalConfig.sharedInstance.speakerImageForColor(color)?.displayColor  {
            return displayColorName
        }
        return ""
    }
}
