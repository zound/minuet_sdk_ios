//
//  AuxViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class AuxViewModel: SelectableViewModel {
    
    var mode: Mode
    
    init (mode: Mode) {
        
        self.mode = mode
    }
    
    weak var delegate: SelectableViewModelDelegate?
    
    var isSelected: Variable<Bool> = Variable(false)
    
    var selectableImageName: String? { return "source_aux_icon" }
    
    var playableItem: PlayableItem? { return mode }
}
