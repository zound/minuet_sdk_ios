//
//  VolumeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import VisualEffectView
import RxDataSources
import Zound

protocol VolumeViewControllerDelegate: class {
    
    func volumeViewControllerDidRequestClose(_ volumeViewController: VolumeViewController)
}

enum Group {
    
    case multi
    case solo
}

class VolumeViewController: UIViewController {
    
    weak var delegate: VolumeViewControllerDelegate?
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionModel<Group, SpeakerVolumeViewModel>>()
    var viewModel: VolumeViewModel?
    @IBOutlet weak var muteAllButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var visualEffectView: VisualEffectView!

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var separatorViewHeight: NSLayoutConstraint!
    @IBOutlet weak var gradientView: UrbanearsBackgroundView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.rx.notification(NSNotification.Name.UIAccessibilityReduceTransparencyStatusDidChange)
            .map{ _ in return UIAccessibilityIsReduceTransparencyEnabled() }
            .startWith(UIAccessibilityIsReduceTransparencyEnabled())
            .subscribe(weak: self, onNext: VolumeViewController.updateBackgroundForReduceTransparencySetting)
            .disposed(by: rx_disposeBag)
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Volume.Title)
        muteAllButton.setAttributedTitle(Fonts.UrbanEars.Regular(10).AttributedTextWithString(Localizations.Volume.Buttons.MuteAll, color: UIColor(white: 1.0, alpha: 0.8), letterSpacing: 1.5), for: .normal)
        
        muteAllButton.titleLabel?.numberOfLines = 3
        
        updateSeparatorViewVisibility()
        separatorViewHeight.constant = 0.9
        
        if let vm = viewModel {
            let dataSource = self.dataSource
            let soloViewModels = vm.soloGroupsViewModels.asObservable().unwrapOptional()
            let multiViewModels = vm.multiGroupsViewModels.asObservable().unwrapOptional()
            let items = Observable.combineLatest(soloViewModels, multiViewModels) { solos, multis -> [SectionModel<Group, SpeakerVolumeViewModel>] in
                var sectionModels = [SectionModel<Group, SpeakerVolumeViewModel>]()
                
                let multiVolumeViewModels = multis.flatMap{ viewModel in viewModel.speakerViewModels.value }
                if multiVolumeViewModels.count > 0 {
                    
                    sectionModels.append(SectionModel(model: Group.multi, items: multiVolumeViewModels))
                }
                
                let soloVolumeViewModels = solos.flatMap{ viewModel in viewModel.speakerViewModels.value }
                if soloVolumeViewModels.count > 0 {
                 
                    sectionModels.append(SectionModel(model: Group.solo, items: soloVolumeViewModels))
                }
                return sectionModels

            }
            
            collectionView.delegate = self
            collectionView.alwaysBounceVertical = true
            
            collectionView.register(UINib(nibName: "SoloVolumeHeaderReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "soloHeader")
            collectionView.register(UINib(nibName: "MultiVolumeHeaderReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "multiHeader")
            collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "empty")
            
            collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "empty")
            
            dataSource.configureCell = { (_, collectionView, indexPath, item) in
                
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "volumeCell", for: indexPath as IndexPath) as? VolumeCell {

                    cell.viewModel = item
                    return cell
                }
                
                return collectionView.dequeueReusableCell(withReuseIdentifier: "empty", for: indexPath as IndexPath)

            }
            dataSource.supplementaryViewFactory = { [weak self] (dataSource, collectionView, kind, indexPath) in
                
                let sectionModel = dataSource[(indexPath as NSIndexPath).section]
                if sectionModel.model == Group.multi {
                    if let section = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "multiHeader", for: indexPath as IndexPath) as? MultiVolumeHeaderReusableView {
                        
                        if let multiGroupViewModels = self?.viewModel?.multiGroupsViewModels.value {
                            if multiGroupViewModels.count > indexPath.section {
                                let groupViewModel = multiGroupViewModels[indexPath.section]
                                section.groupViewModel = groupViewModel
                            }
                            
                        }
                        section.backgroundColor = UIColor.clear
                        return section
                    }
                }
                
                return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "empty", for: indexPath as IndexPath)
            }
            
            items.bind(to:collectionView.rx.items(dataSource:dataSource)).disposed(by: rx_disposeBag)
            
            vm.globalMute.asObservable().subscribe(onNext: { [weak self] mute in
                self?.upateForGlobalMute(mute)
            }).disposed(by: rx_disposeBag)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.superview?.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.clear
    }
    
    func updateBackgroundForReduceTransparencySetting(_ reduceTransparency: Bool) {
        
        if !reduceTransparency {
        
            visualEffectView.isHidden = false
            visualEffectView.backgroundColor = UIColor.clear
            visualEffectView.colorTint = UIColor("#333333", defaultColor: UIColor.black)
            visualEffectView.colorTintAlpha = 0.4
            visualEffectView.blurRadius = 15
            visualEffectView.scale = 1
            gradientView.alpha = 0.5
            view.backgroundColor = UIColor.clear
            
        
        } else {
            
            visualEffectView.isHidden = true
            view.backgroundColor = UIColor.black
            gradientView.alpha = 1.0
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func updateSeparatorViewVisibility() {
        
        separatorView.alpha = max(0,min(1.0,collectionView.contentOffset.y/22.0))
    }
    
    func upateForGlobalMute(_ globalMute: Bool) {
        
    
            UIView.setAnimationsEnabled(false)
            if globalMute {
                 muteAllButton.setAttributedTitle(Fonts.UrbanEars.Regular(10).AttributedTextWithString(Localizations.Volume.Buttons.UnmuteAll, color: UIColor(white: 1.0, alpha: 0.8), letterSpacing: 1.5), for: .normal)
                
            } else {
                 muteAllButton.setAttributedTitle(Fonts.UrbanEars.Regular(10).AttributedTextWithString(Localizations.Volume.Buttons.MuteAll, color: UIColor(white: 1.0, alpha: 0.8), letterSpacing: 1.5), for: .normal)
            }
            muteAllButton.layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.itemSize = CGSize(width: self.collectionView.bounds.width-self.collectionView.contentInset.left-self.collectionView.contentInset.right, height: flowLayout.itemSize.height)
    }
    
    @IBAction func onMuteAll(_ sender: AnyObject) {
        
        viewModel?.muteAll()
    }
    @IBAction func onClose(_ sender: AnyObject) {
        
        self.delegate?.volumeViewControllerDidRequestClose(self)
    }
}

extension VolumeViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.updateSeparatorViewVisibility()
    }
    
}

extension VolumeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let sectionModel = dataSource[section]
        if sectionModel.model == Group.multi {
            
            return CGSize(width: 320, height: 70)
        } else if sectionModel.model == Group.solo {
            return CGSize(width: 320, height: 0)
        }
        
        return CGSize.zero
    }
}


