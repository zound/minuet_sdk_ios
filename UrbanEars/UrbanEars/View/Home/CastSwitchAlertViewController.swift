//
//  VolumeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Zound

protocol CastSwitchAlertViewControllerDelegate: class {
    
    func castSwitchAlertDidRequestContinue(_ alertViewController: CastSwitchAlertViewController)
    func castSwitchAlertDidRequestCancel(_ alertViewController: CastSwitchAlertViewController)
}


class CastSwitchAlertViewController: UIViewController, BlurViewController {
    
    var dontShow: Bool = false
    weak var delegate: CastSwitchAlertViewControllerDelegate?
    var speakerViewModel: HomeSpeakerViewModel?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var learnMoreButton: UIButton!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Appwide.Note
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentLabel.text = Localizations.CastSwitch.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)

        learnMoreButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide._Continue), for: .normal)
        
        continueButton.layoutIfNeeded()
        learnMoreButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onClose(_ sender: AnyObject) {
        
        self.delegate?.castSwitchAlertDidRequestCancel(self)
    }
    @IBAction func onContinue(_ sender: AnyObject) {
        
        self.delegate?.castSwitchAlertDidRequestContinue(self)
    }
    @IBAction func onLearnMore(_ sender: AnyObject) {
        
        self.delegate?.castSwitchAlertDidRequestCancel(self)
    }
}
