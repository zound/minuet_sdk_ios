//
//  NoWiFiViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol NoWiFiViewControllerDelegate: class {
    
    func noWifiViewControllerDidRequestMenu(_ noWifiViewController: UIViewController)
}

class NoWiFiViewController: UIViewController {
    
    
    weak var delegate: NoWiFiViewControllerDelegate?
    
    @IBOutlet weak var messageTitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var goToSettingsButton : UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTitleLabel.attributedText = Fonts.UrbanEars.ExtraLight(50).AttributedTextWithString(Localizations.NoWifi.Title, color: UIColor.white, letterSpacing: -1.0)
        
        goToSettingsButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.NoWifi.Buttons.GoToSettings), for: .normal)

        messageLabel.text = Localizations.NoWifi.Description
        messageLabel.font = Fonts.MainContentFont
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onMenu(_ sender: AnyObject) {
        
        self.delegate?.noWifiViewControllerDidRequestMenu(self)
    }
    
    @IBAction func onSettings(_ sender : AnyObject) {
        guard let settingsURL = URL(string: UIApplicationOpenSettingsURLString) else { return }
        guard UIApplication.shared.canOpenURL(settingsURL) else { return }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsURL)
        } else {
            UIApplication.shared.openURL(settingsURL)
        }
    }
}
