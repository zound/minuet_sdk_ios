//
//  SettingsSettingCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class SettingsCastSettingCell: UITableViewCell {

    let settingVariable: Variable<CastSetting?> = Variable(nil)
    var setting: CastSetting? {
        
        get {
            return settingVariable.value
        }
        set {
            settingVariable.value = newValue
        }
    }
    @IBOutlet weak var settingLabel: UILabel!
    
    override func awakeFromNib() {
        
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        settingLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
        
        settingVariable.asObservable().map{ $0 != nil ?  $0!.localizedDisplayName : "" }.subscribe(onNext: { [weak self] displayName in
            self?.settingLabel.attributedText = Fonts.ListItemFont.AttributedTextWithString(displayName, color: self!.settingLabel.textColor, letterSpacing: 0, lineSpacing: -10.0, lineHeightMultiple: 0.8)
        }).disposed(by: rx_disposeBag)
    }
}
