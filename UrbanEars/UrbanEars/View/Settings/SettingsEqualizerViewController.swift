//
//  SettingsEqualizer.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsEqualizerViewControllerDelegate: class {
    
    func settingsEqualizerViewControllerDidRequestBack(_ viewController: SettingsEqualizerViewController)
}


class SettingsEqualizerViewController: UIViewController {
    
    weak var delegate: SettingsEqualizerViewControllerDelegate?
    var viewModel: SettingsEqualizerViewModel?

    @IBOutlet weak var bassContainer: UIView!
    @IBOutlet weak var bassSlider: UISlider!
    @IBOutlet weak var trebleContainer: UIView!
    @IBOutlet weak var trebleSlider: UISlider!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bassLabel: UILabel!
    @IBOutlet weak var trebleLabel: UILabel!
    @IBOutlet weak var resetEqButton: UIButton!
    @IBOutlet weak var label0: UILabel!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label10: UILabel!
    
    override func viewDidLoad() {
        
        bassSlider.setMinimumTrackImage(UIImage(named:"eq_slider_track_min"), for: .normal)
        bassSlider.setMaximumTrackImage(UIImage(named:"eq_slider_track_max"), for: .normal)
        
        trebleSlider.setMinimumTrackImage(UIImage(named:"eq_slider_track_min"), for: .normal)
        trebleSlider.setMaximumTrackImage(UIImage(named:"eq_slider_track_max"), for: .normal)
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.Equalizer.Title)
        bassLabel.attributedText = Fonts.UrbanEars.Regular(11).AttributedTextWithString(Localizations.Settings.Equalizer.Bass, color: UIColor(white:1.0, alpha: 0.7), letterSpacing: 2.0)
        trebleLabel.attributedText = Fonts.UrbanEars.Regular(11).AttributedTextWithString(Localizations.Settings.Equalizer.Treble, color: UIColor(white:1.0, alpha: 0.7), letterSpacing: 2.0)
        
        
        UIView.setAnimationsEnabled(false)
        resetEqButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Settings.Equalizer.ResetEq), for: .normal)
        resetEqButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        label0.attributedText = Fonts.UrbanEars.Regular(11).AttributedTextWithString("0", color: UIColor(white:1.0, alpha: 0.7), letterSpacing: 2.0)
        label5.attributedText = Fonts.UrbanEars.Regular(11).AttributedTextWithString("5", color: UIColor(white:1.0, alpha: 0.7), letterSpacing: 2.0)
        label10.attributedText = Fonts.UrbanEars.Regular(11).AttributedTextWithString("10", color: UIColor(white:1.0, alpha: 0.7), letterSpacing: 2.0)
        
        bassSlider.setThumbImage(UIImage(named: "small_volume_slider_thumb"), for: .normal)
        trebleSlider.setThumbImage(UIImage(named: "small_volume_slider_thumb"), for: .normal)
        
        
        if let vm = viewModel {
            
            vm.bass.asObservable().bind(to:bassSlider.rx.value).disposed(by: rx_disposeBag)
            vm.treble.asObservable().bind(to:trebleSlider.rx.value).disposed(by: rx_disposeBag)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    @IBAction func onUpdatedBassValue(_ sender: AnyObject) {
        
        if let vm = viewModel {
            
            vm.setBassToValue(bassSlider.value)
        }
    }
    
    @IBAction func onUpdatedTrebleValue(_ sender: AnyObject) {
    
        if let vm = viewModel {
            
            vm.setTrebleToValue(trebleSlider.value)
        }
    }
    
    @IBAction func onResetEq(_ sender: AnyObject) {
        
        if let vm = viewModel {
            
            vm.resetEq()
        }
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.settingsEqualizerViewControllerDidRequestBack(self)
    }
}
