//
//  SettingsSpeakerList.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import MinuetSDK

protocol SettingsSpeakerListViewControllerDelegate: class {
    
    func settingsSpeakerListViewControllerDidRequestBack(_ viewController: SettingsSpeakerListViewController)
    func settingsSpeakerListViewControllerDidSelectSpeaker(_ speaker: Speaker)
    func settingsSpeakerListViewControllerDidSelectStreamingQuality(_ cell: SettingsStreamingQualityCell, speakers: [Speaker])
}

class SettingsSpeakerListViewController: UIViewController {
    
    weak var delegate: SettingsSpeakerListViewControllerDelegate?
    var viewModel: SettingsSpeakerListViewModel?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.RootMenu.SoloSpeakers.uppercased())
        tableView.tableFooterView = UIView()
        
        self.tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        
        if let count = self.navigationController?.viewControllers.count, count > 1 {
            
            self.backButton.setImage(UIImage(named: "back"), for: .normal)
        } else {
            self.backButton.setImage(UIImage(named: "close"), for: .normal)
        }

        
        if let vm = viewModel {
            
            vm.speakers.asObservable().subscribe(onNext: { [weak self] cells in
                self?.tableView.reloadData()
            }).disposed(by: rx_disposeBag)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.settingsSpeakerListViewControllerDidRequestBack(self)
    }
    
}

extension SettingsSpeakerListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vm = viewModel {
            let speaker = vm.speakers.value[indexPath.row]
            if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable,
                connectable == true {
                delegate?.settingsSpeakerListViewControllerDidSelectSpeaker(speaker)
            }

        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }

}

extension SettingsSpeakerListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let vm = viewModel {
            return vm.speakers.value.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let vm = viewModel {
            let speaker = vm.speakers.value[indexPath.row]
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "settingsSpeakerCell", for: indexPath) as! SettingsSpeakerCell
            cell.viewModel = speaker
            return cell
        }
        return UITableViewCell()
    }
    
}
