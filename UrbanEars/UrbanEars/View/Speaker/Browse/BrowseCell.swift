//
//  BrowseCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import MinuetSDK

class BrowseCell: UITableViewCell {
    
    let item: Variable<NavListItem?> = Variable(nil)
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        nameLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
        
        item.asObservable().map{ $0 != nil ?  nameForItem($0!): "" }.bind(to:nameLabel.rx.text).disposed(by: rx_disposeBag)
    }
    
}

func nameForItem(_ item: NavListItem) -> String {
    
    if item.type == NavListType.playableItem {
        
        return  [String(item.key+1),". ", item.name].joined(separator: "")
    }
    
    return item.name
}
