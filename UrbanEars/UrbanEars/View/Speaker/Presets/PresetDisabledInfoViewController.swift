//
//  PresetDisabledInfoViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import Zound

protocol PresetDisabledInfoViewControllerDelegate: class {
    
    func presetDisabledAlertDidRequestBack(_ alertViewController: PresetDisabledInfoViewController)
    func presetDisabledAlertDidRequestLearnMore(_ alertViewController: PresetDisabledInfoViewController)
}

class PresetDisabledInfoViewController: UIViewController, BlurViewController {
 

    weak var delegate: PresetDisabledInfoViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var backButon: UIButton!
    @IBOutlet weak var readMoreButton: UIButton!   
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.text = Localizations.PresetsDisabled.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        let contentFont = Fonts.MainContentFont
        let contentColor = UIColor("#FFFFFF")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 5
        
        let termsAttributes = [NSFontAttributeName:contentFont,
                               NSForegroundColorAttributeName:contentColor,
                               NSParagraphStyleAttributeName: paragraphStyle]
        
        let contentText = Localizations.PresetsDisabled.Content
        let contentAttributedString = NSMutableAttributedString(string: contentText, attributes: termsAttributes)
        let imageTokens = ["[cloud_image]": UIImage (named: "preset_disabled_cloud_icon"),
                           "[plus_image]": UIImage (named: "preset_add_button")]
        
        for token in imageTokens.keys {
            let tokenRange = (contentAttributedString.string as NSString).range(of: token)
            if tokenRange.length != 0 {
                let replaceImage = imageTokens[token]!
                
                let textAttachment = NSTextAttachment()
                textAttachment.image = replaceImage
                let attrStringWithImage = NSAttributedString(attachment: textAttachment)
                contentAttributedString.replaceCharacters(in: tokenRange, with: attrStringWithImage)
            }
        }
        
        contentLabel.attributedText = contentAttributedString
        contentLabel.font = Fonts.MainContentFont
        
        backButon.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        readMoreButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.PresetsDisabled.Buttons.ReadMore), for: .normal)
    }
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.presetDisabledAlertDidRequestBack(self)
    }
    @IBAction func onLearnMore(_ sender: AnyObject) {
        
        self.delegate?.presetDisabledAlertDidRequestLearnMore(self)
    }
}
