//
//  AuxSelectableViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

class AuxSelectableViewController: SelectableViewController {
    
    @IBOutlet weak var auxinActiveContainer: UIView!
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var auxActiveLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activateButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Aux.Activate), for: .normal)
        auxActiveLabel.attributedText = Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Player.Aux.Activated)
        
        self.view.backgroundColor = UIColor.clear
        
        let isSelected = viewModelVariable.asObservable().map{ ($0 as! AuxViewModel).isSelected.asObservable() }.switchLatest()
        isSelected.distinctUntilChanged()
            .skip(1)
            .subscribe(weak: self, onNext: AuxSelectableViewController.updateForActive)
            .disposed(by: rx_disposeBag)
        
        isSelected.take(1)
            .subscribe(onNext: { [weak self] active in
                self?.updateForActive(active, animated: false)
            }).disposed(by: rx_disposeBag)
    }
    
    override var relativeDistanceFromCenter: Double {
        
        didSet {
            
            self.view.alpha = 1.0 - fmin(fabs(CGFloat(relativeDistanceFromCenter)),1.0)
        }
    }
    
    
    func updateForActive(_ active: Bool) {
        
        updateForActive(active, animated: true)
    }
    
    func updateForActive(_ active: Bool, animated: Bool) {
        
        if active {
            UIView.setAlphaOfView(auxinActiveContainer, visible: true, animated: animated)
            UIView.setAlphaOfView(activateButton, visible: false, animated: animated)
        } else {
            
            UIView.setAlphaOfView(auxinActiveContainer, visible: false, animated: animated)
            UIView.setAlphaOfView(activateButton, visible: true, animated: animated)
        }
    }
    
    @IBAction func onActivate(_ sender: AnyObject) {
        
        if let vm = viewModel as? AuxViewModel {
            
            vm.select(vm.mode)
        }
    }
}
