//
//  CloudSelectableViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct CloudSource {
    
    let name: String
    let imageName: String
}

protocol CloudSelectableViewControllerDelegate: class {
    func cloudViewControllerDidRequestSpotifyConnect(_ viewController: CloudSelectableViewController)
    func cloudViewControllerDidRequestGoogleCast(_ viewController: CloudSelectableViewController)
    func cloudViewControllerDidRequestAirplay(_ viewController: CloudSelectableViewController)
    func cloudViewControllerDidRequestBrowseRadio(_ viewController: CloudSelectableViewController, view: UIView)
}

class CloudSelectableViewController: SelectableViewController {


    @IBOutlet weak var spotifyConnectButton: UIButton!
    @IBOutlet weak var playInternetRadioButton: UIButton!
    @IBOutlet weak var googleCastButton: UIButton!
    @IBOutlet weak var airplayButton: UIButton!
    @IBOutlet weak var orPlayButton: UIButton!
    
    
    weak var delegate: CloudSelectableViewControllerDelegate?
    
    override var relativeDistanceFromCenter: Double {
        
        didSet {
            
            self.view.alpha = 1.0 - fmin(fabs(CGFloat(relativeDistanceFromCenter)),1.0)
        }
    }
    @IBAction func onPlayRadio(_ sender: AnyObject) {
        
        self.delegate?.cloudViewControllerDidRequestBrowseRadio(self, view: sender as! UIButton)
    }
    @IBAction func onSpotifyConnect(_ sender: AnyObject) {
        
        self.delegate?.cloudViewControllerDidRequestSpotifyConnect(self)
    }
    @IBAction func onGoogleCast(_ sender: AnyObject) {
        
        self.delegate?.cloudViewControllerDidRequestGoogleCast(self)
    }
    @IBAction func onAirplay(_ sender: AnyObject) {
        
        self.delegate?.cloudViewControllerDidRequestAirplay(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIView.setAnimationsEnabled(false)
        
        
        playInternetRadioButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Cloud.PlayInternetRadio), for: .normal)
        
        orPlayButton.setTitle(Localizations.Player.Cloud.OrPlayMobileApp, for: .normal)
        orPlayButton.titleLabel?.font = Fonts.UrbanEars.Regular(17)
        orPlayButton.titleLabel?.numberOfLines = 2
        orPlayButton.titleLabel?.textAlignment = .center
        
        spotifyConnectButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Player.Cloud.SpotifyConnect), for: .normal)
        googleCastButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Player.Cloud.GoogleCast), for: .normal)
        airplayButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Player.Cloud.Airplay), for: .normal)
        
        playInternetRadioButton.layoutIfNeeded()
        orPlayButton.layoutIfNeeded()
        spotifyConnectButton.layoutIfNeeded()
        googleCastButton.layoutIfNeeded()
        airplayButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.view.backgroundColor = UIColor.clear
    }
}

