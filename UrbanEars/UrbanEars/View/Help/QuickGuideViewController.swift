//
//  AboutViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol QuickGuideViewControllerDelegate: class {
    
    func quickGuideViewControllerDidRequestBack(_ quickGuideViewController: QuickGuideViewController)
    func quickGuideViewControllerDidRequestQuickGuideItem(_ guickGuideItem: QuickGuideItem, _ quickGuideViewController: QuickGuideViewController)
}

enum QuickGuideItem {
    
    case speakerKnobs
    case presets
    case soloMulti
    
    var localizedDisplayName: String {
        switch  self {
        case .speakerKnobs: return Localizations.Help.QuickGuide.MenuItem.SpeakerKnobs
        case .presets: return Localizations.Help.QuickGuide.MenuItem.Presets
        case .soloMulti: return Localizations.Help.QuickGuide.MenuItem.SoloMulti
        }
    }
    
    var localWebPageURL: URL? {
        
        var pageName:String? = nil
        switch self {
        case .speakerKnobs: pageName = "speaker_knobs"
        case .presets: pageName = "presets"
        case .soloMulti: pageName = "solo_multi"
        }
        var subdirectory = "QuickGuide/en"
        var language = NSLocale.preferredLanguages[0]
        if language.count > 2 {
            let index = language.index(language.startIndex, offsetBy: 2)
            language = language.substring(to: index)
        }
        subdirectory = "QuickGuide/\(language)"

        
        if let url = Bundle.main.url(forResource: pageName, withExtension: "html", subdirectory: subdirectory) {
            return url
        } else {
            return Bundle.main.url(forResource: pageName, withExtension: "html", subdirectory: "QuickGuide/en")
        }
        
    }

}

class QuickGuideViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!

    
    weak var delegate: QuickGuideViewControllerDelegate?
    
    let menuItems: [QuickGuideItem]
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.speakerKnobs, .presets, .soloMulti]
        super.init(coder: aDecoder)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.quickGuideViewControllerDidRequestBack(self)
    }
    
    override func viewDidLoad() {
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Help.QuickGuide.Title.uppercased())
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}


extension QuickGuideViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuItem = menuItems[(indexPath as NSIndexPath).row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "quickGuideItem", for: indexPath) as? QuickGuideCell {
            cell.quickGuideItem = menuItem
            return cell
        }
        
        return UITableViewCell()
    }
    
}
extension QuickGuideViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let quickMenuItem = menuItems[(indexPath as NSIndexPath).row]
        
        self.delegate?.quickGuideViewControllerDidRequestQuickGuideItem(quickMenuItem, self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
