//
//  AboutViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol HelpViewControllerDelegate: class {
    
    func helpViewControllerDidRequestBack(_ helpViewController: HelpViewController)
    func helpViewControllerDidRequestHelpItem(_ helpItem: HelpItem, _ helpViewController: HelpViewController)
}

enum HelpItem {
    
    case quickGuide
    case onlineManual
    case contact
    
    var localizedDisplayName: String {
        switch  self {
        case .quickGuide: return Localizations.Help.MenuItem.QuickGuide
        case .onlineManual: return Localizations.Help.MenuItem.OnlineManual
        case .contact: return Localizations.Help.MenuItem.Contact
        }
    }
}

class HelpViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!

    
    weak var delegate: HelpViewControllerDelegate?
    
    let menuItems: [HelpItem]
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.quickGuide, .onlineManual, .contact]
        super.init(coder: aDecoder)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.helpViewControllerDidRequestBack(self)
    }
    
    override func viewDidLoad() {
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Help.Title.uppercased())
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}



extension HelpViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuItem = menuItems[(indexPath as NSIndexPath).row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "helpItem", for: indexPath) as? HelpItemCell {
            cell.helpItem = menuItem
            return cell
        }
        
        return UITableViewCell()
    }
    
}
extension HelpViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let menuItem = menuItems[(indexPath as NSIndexPath).row]
        self.delegate?.helpViewControllerDidRequestHelpItem(menuItem, self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
