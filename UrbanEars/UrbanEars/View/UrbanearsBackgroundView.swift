//
//  UrbanearsBackgroundView.swift
//  Urbanears Background View
//
//  Created by Sam Soffes on 10/27/09.
//  Copyright (c) 2009-2014 Sam Soffes. All rights reserved.
//

import UIKit

/// Simple view for drawing gradients and borders.
open class UrbanearsBackgroundView: UIView {
    
    
    @IBInspectable open var topColor: UIColor? = UIColor("#6C6C6C") {
        didSet {
            updateGradient()
        }
    }
    @IBInspectable open var bottomColor: UIColor? = UIColor("#3C3C3C") {
        didSet {
            updateGradient()
        }
    }
    
    /// An optional array of `CGFloat`s defining the location of each gradient stop.
    ///
    /// The gradient stops are specified as values between `0` and `1`. The values must be monotonically increasing. If
    /// `nil`, the stops are spread uniformly across the range.
    ///
    /// Defaults to `nil`.
    open var locations: [CGFloat]? {
        didSet {
            updateGradient()
        }
    }
    
    
    // MARK: - UIView
    
    override open func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        // Let's make sure that every asset is loaded, because we don't want to display half of the gradient or something else
        // that would cause an artifact on the screen
        guard let image = UIImage(named: "background_noise"),
            let ciImage = CIImage(image: image),
            let cgImage = CIContext(options: nil).createCGImage(ciImage, from: ciImage.extent) else {
                return
        }
        
        // Update gradient
        if gradient == nil {
            updateGradient()
        }
        
        if let gradient = gradient {
            let size = bounds.size
            let scale = UIScreen.main.scale
            let options: CGGradientDrawingOptions = [.drawsAfterEndLocation]
            
            let startPoint = CGPoint.zero
            let endPoint = CGPoint(x: 0, y: size.height)
            
            context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: options)
            
            let drawRect = CGRect(x: 0, y: 0, width: CGFloat(cgImage.width) / scale, height: CGFloat(cgImage.height) / scale);
            context.draw(cgImage, in: drawRect, byTiling: true)
        }
    }
    
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        contentMode = .redraw
    }
    
    
    // MARK: - Private
    
    fileprivate var gradient: CGGradient?
    
    fileprivate func updateGradient() {
        gradient = nil
        setNeedsDisplay()
        
        let colors = gradientColors()
        if let colors = colors {
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let colorSpaceModel = colorSpace.model
            
            let gradientColors = colors.map { (color: UIColor) -> AnyObject! in
                let cgColor = color.cgColor
                let cgColorSpace = cgColor.colorSpace ?? colorSpace
                
                // The color's color space is RGB, simply add it.
                if cgColorSpace.model == colorSpaceModel {
                    return cgColor as AnyObject!
                }
                
                // Convert to RGB. There may be a more efficient way to do this.
                var red: CGFloat = 0
                var blue: CGFloat = 0
                var green: CGFloat = 0
                var alpha: CGFloat = 0
                color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
                return UIColor(red: red, green: green, blue: blue, alpha: alpha).cgColor as AnyObject!
                } as NSArray
            
            gradient = CGGradient(colorsSpace: colorSpace, colors: gradientColors, locations: locations)
        }
    }
    
    fileprivate func gradientColors() -> [UIColor]? {
        
        return [topColor ?? UIColor.white, bottomColor ?? UIColor.black]
    }
}
