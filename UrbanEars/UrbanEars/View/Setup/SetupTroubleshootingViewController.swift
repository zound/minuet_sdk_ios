//
//  SetupConfiguringViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 25/01/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Crashlytics

protocol SetupTroubleshootingViewControllerDelegate:class {
    

    func setupTroubleshootingDidRequestBack(_ setupTroubleshootingViewController:SetupTroubleshootingViewController)
    func setupTroubleshootingDidRequestContactSupport(_ setupTroubleshootingViewController:SetupTroubleshootingViewController)
}

class SetupTroubleshootingViewController: UIViewController {
    
    
    
    @IBOutlet weak var resartSetupButton: UIButton!
    @IBOutlet weak var contactSupportButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var troubleshootingTitle: UILabel!
    var viewModel : SetupNetworkViewModel!
    weak var delegate: SetupTroubleshootingViewControllerDelegate?
    
    @IBOutlet weak var troubleShootingContent1: UILabel!
    @IBOutlet weak var troubleShootingContent2: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        troubleshootingTitle.text = Localizations.Setup.Troubleshooting.Title
        troubleshootingTitle.font = Fonts.UrbanEars.Medium(23)
        
        troubleShootingContent1.text = Localizations.Setup.Troubleshooting.Content1
        troubleShootingContent1.font = Fonts.UrbanEars.Regular(17)
        
        var content2:[String] = []
        content2.append("1.\(Localizations.Setup.Troubleshooting.Content2.Item1)")
        content2.append("2.\(Localizations.Setup.Troubleshooting.Content2.Item2)")
        content2.append("3.\(Localizations.Setup.Troubleshooting.Content2.Item3)")
        content2.append("4.\(Localizations.Setup.Troubleshooting.Content2.Item4)")
        content2.append("5.\(Localizations.Setup.Troubleshooting.Content2.Item5)")
        troubleShootingContent2.text = content2.joined(separator: "\n")
        troubleShootingContent2.font = Fonts.UrbanEars.Regular(17)
        
        UIView.setAnimationsEnabled(false)
        resartSetupButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.Troubleshooting.Buttons.RestartSetup), for: .normal)
        contactSupportButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.Troubleshooting.Buttons.ContactSupport), for: .normal)
        cancelButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        resartSetupButton.layoutIfNeeded()
        cancelButton.layoutIfNeeded()
        contactSupportButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)

        
    }
    @IBAction func onRestartSetupButton(_ sender: Any) {
        
        if let vm = viewModel {
            
            vm.restartConfiguration()
        }

    }
    
    @IBAction func onBack(_ sender: Any){
        
        self.delegate?.setupTroubleshootingDidRequestBack(self)
    }
    
    @IBAction func onContactSupport(_ sender: Any) {
     
        self.delegate?.setupTroubleshootingDidRequestContactSupport(self)
    }
    
}
