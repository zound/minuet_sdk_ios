//
//  FinalizingViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography
import Zound

protocol FinalizingViewControllerDelegate: class {
    
    func finalizingViewControllerDidRequestBack(_ viewController:FinalizingViewController)
    func finalizingViewControllerFinishSetup(_ viewController:FinalizingViewController)
}

enum FinalizingViewState {
    
    case empty, settingUp, downloadUpdates, installingUpdate, done
}

class FinalizingViewController: UIViewController {

    weak var delegate: FinalizingViewControllerDelegate?
    
    @IBOutlet weak var progressContainer: UrbanearsBackgroundView!
    @IBOutlet weak var spinnerView: SpinnerView!
    @IBOutlet weak var currentStatusLabel: UILabel!
    @IBOutlet weak var currentStatusDescription: UILabel!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var completeTickImage: UIImageView!
    @IBOutlet weak var completeText: UILabel!
    
    var progressContainerPositionConstraint : ConstraintGroup?
    var completedViewsConstraints : ConstraintGroup?
    
    func transitionToState(_ state: FinalizingViewState, animated:Bool) {
        
        switch state {
        case .empty:
            
            setProgressContainerVisible(false, animated: animated)
            setCompletedViewsVisible(false, animated: animated)
            currentStatusLabel.text = nil
            currentStatusDescription.text = nil
            
        case .settingUp:
            
            setProgressContainerVisible(true, animated: animated)
            setCompletedViewsVisible(false, animated: animated)
            currentStatusLabel.text = "Setting up \"Living Room\""
            currentStatusDescription.text = "This might take a few minutes"

        case .downloadUpdates:

            setProgressContainerVisible(true, animated: animated)
            setCompletedViewsVisible(false, animated: animated)
            currentStatusLabel.text = "Downloading update"
            currentStatusDescription.text = "46% downloaded. Hang on"
            
        case .installingUpdate:

            setProgressContainerVisible(true, animated: animated)
            setCompletedViewsVisible(false, animated: animated)
            currentStatusLabel.text = "Installing update"
            currentStatusDescription.text = "Your speaker is getting the latest software"
            
        case .done:
            
            setProgressContainerVisible(false, animated: animated)
            setCompletedViewsVisible(true, animated: animated)
            currentStatusLabel.text = nil
            currentStatusDescription.text = nil
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func setProgressContainerVisible(_ visible: Bool, animated: Bool) {

        let constraintsToReplace = progressContainerPositionConstraint ?? ConstraintGroup()
        progressContainerPositionConstraint = constrain(progressContainer, replace: constraintsToReplace) { view in
            if visible {
                view.bottom == view.superview!.bottom
            } else {
                view.top == view.superview!.bottom
            }
        }
        if animated {
            
            UIView.animate(withDuration: 0.25, animations: view.layoutIfNeeded)
        } else {
            
            view.layoutIfNeeded()
        }
    }
    
    
    func setCompletedViewsVisible(_ visible: Bool, animated: Bool) {

        let constraintsToReplace = completedViewsConstraints ?? ConstraintGroup()
        completedViewsConstraints = constrain(completeTickImage, completeText, finishButton, replace: constraintsToReplace) { completeTickImage ,completeText ,finishButton in
            
            if visible {
                completeTickImage.centerY == completeTickImage.superview!.centerY
                finishButton.bottom == finishButton.superview!.bottom-77
            } else {
                completeTickImage.centerY == completeTickImage.superview!.centerY+50
                finishButton.bottom == finishButton.superview!.bottom-20
            }
        }
        
        let setOpacity = { [weak self] in
            if(visible) {
                self?.completeTickImage.alpha = 1.0
                self?.completeText.alpha = 1.0
                self?.finishButton.alpha = 1.0
            } else {
                self?.completeTickImage.alpha = 0.0
                self?.completeText.alpha = 0.0
                self?.finishButton.alpha = 0.0
            }
        }
        
        if animated {
            
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                setOpacity()
                
            })
        } else {
            
            view.layoutIfNeeded()
            setOpacity()
        }
        
    }
    
    @IBAction func onBack(_ sender: AnyObject) {

        self.delegate?.finalizingViewControllerDidRequestBack(self)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        transitionToState(.empty, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        transitionToState(.settingUp, animated: true)
        runAfterDelay(1.0, block: { [weak self] in
            self?.transitionToState(.settingUp, animated: true)
        })
        runAfterDelay(2.0, block: { [weak self] in
            self?.transitionToState(.downloadUpdates, animated: true)
        })
        runAfterDelay(3.0, block: { [weak self] in
            self?.transitionToState(.installingUpdate, animated: true)
        })
        runAfterDelay(5.0, block: { [weak self] in
            self?.transitionToState(.done, animated: true)
        })
    }
    
    @IBAction func onFinish(_ sender: AnyObject) {
        
        self.delegate?.finalizingViewControllerFinishSetup(self)
    }
    
}
