//
//  SetupConfiguringViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 25/01/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol SetupWACFailureViewControllerDelegate:class {
    
    func setupWACFailureDidRequestCancelSetup(_ setupWACFailureViewController: SetupWACFailureViewController)
    func setupWACFailureDidRequestCloseApp(_ setupWACFailureViewController:SetupWACFailureViewController)
}

class SetupWACFailureViewController: UIViewController {
    
    
  
    var viewModel : SetupNetworkViewModel!
    weak var delegate: SetupWACFailureViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var closeAppButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.WacFailure.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentLabel.text = Localizations.Setup.WacFailure.Content
        contentLabel.font = Fonts.UrbanEars.Regular(17)
        
        UIView.setAnimationsEnabled(false)
        
        closeAppButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.WacFailure.Buttons.CloseApp), for: .normal)
        cancelButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        closeAppButton.layoutIfNeeded()
        cancelButton.layoutIfNeeded()
        
        UIView.setAnimationsEnabled(true)
        
    }
    @IBAction func onCloseSetup(_ sender: Any) {
        
        self.delegate?.setupWACFailureDidRequestCancelSetup(self)
    }
    @IBAction func onCloseApp(_ sender: Any) {
        
           self.delegate?.setupWACFailureDidRequestCloseApp(self)
    }
    
}
