//
//  SetupFailedSetupViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SetupFailedSetupViewControllerDelegate: class {
    
    func setupFailedSetupDidConfirmSetupMode(_ viewController: SetupFailedSetupViewController)
    func setupFailedSetupDidDenySetupMode(_ viewController: SetupFailedSetupViewController)
}

class SetupFailedSetupViewController: UIViewController {
    
    weak var delegate: SetupFailedSetupViewControllerDelegate?
    @IBOutlet weak var dialImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        messageLabel.text = Localizations.Setup.FailedSetup.Message
        messageLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        yesButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.FailedSetup.Buttons.Yes), for: .normal)
        noButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Setup.FailedSetup.Buttons.No), for: .normal)
        
        yesButton.layoutIfNeeded()
        noButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        
        self.view.backgroundColor = UIColor.clear
        
        dialImage.animationImages = [UIImage(named:"setup_ap_mode_leds")!, UIImage(named:"setup_ap_mode_leds_off")!]
        dialImage.animationDuration = 2.0
        dialImage.animationRepeatCount = 0
        dialImage.startAnimating()
    }
    
    @IBAction func onYes(_ sender: AnyObject) {
        
        self.delegate?.setupFailedSetupDidConfirmSetupMode(self)
    }
    @IBAction func onNo(_ sender: AnyObject) {
        
        self.delegate?.setupFailedSetupDidDenySetupMode(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}
