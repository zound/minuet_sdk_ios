//
//  SetupConfiguringViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 25/01/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol SetupLostConnectionViewControllerDelegate:class {
    
    
    func setupLostConnectionDidRequestReconnect(_ setupLostConnectionViewController: SetupLostConnectionViewController)
    func setupLostConnectionDidRequestTroubleshooting(_ setupLostConnectionViewController: SetupLostConnectionViewController)
    func setupLostConnectionDidRequestClose(_ setupLostConnectionViewController:SetupLostConnectionViewController)
}

class SetupLostConnectionViewController: UIViewController {
    
    
    var viewModel : SetupNetworkViewModel!
    weak var delegate: SetupLostConnectionViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var reconnectButton: UIButton!
    @IBOutlet weak var troubleshootingButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.LostConnection.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentLabel.text = Localizations.Setup.LostConnection.Content
        contentLabel.font = Fonts.UrbanEars.Regular(17)
        
        UIView.setAnimationsEnabled(false)
        
        reconnectButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.LostConnection.Buttons.Reconnect), for: .normal)
        reconnectButton.layoutIfNeeded()
        
        troubleshootingButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.LostConnection.Buttons.Troubleshooting), for: .normal)
        troubleshootingButton.layoutIfNeeded()
        
        cancelButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        
        UIView.setAnimationsEnabled(true)
        
    }
    @IBAction func onCloseSetup(_ sender: Any) {
        
        self.delegate?.setupLostConnectionDidRequestClose(self)
    }
    
    @IBAction func onReconnect(_ sender: Any) {
        
        if let vm = viewModel {
            
            vm.retryDiscovery()
            self.delegate?.setupLostConnectionDidRequestReconnect(self)
        }
    }

    @IBAction func onTroubleshooting(_ sender: Any) {
        
        self.delegate?.setupLostConnectionDidRequestTroubleshooting(self)
    }
}
