//
//  SetupConfiguringViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 25/01/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import MinuetSDK

protocol SetupConfiguringViewControllerDelegate:class {
    
    func setupConfiguringDidRequestTroubleshooting(_ setupConfiguringViewController: SetupConfiguringViewController)
    func setupConfiguringDidRequestCancel(_ setupListViewController:SetupListViewController)
}

class SetupConfiguringViewController: UIViewController {
    
    
    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var spinnerImage: UIImageView!
    var viewModel : SetupNetworkViewModel!
    weak var delegate: SetupConfiguringViewControllerDelegate?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        spinnerImage.rotate(0.5)
        makeLoading(visible: false, animated: false)
        if let vm = viewModel {
            
            Observable.combineLatest(
            vm.setupNetworkStateVariable.asObservable(),
            vm.speakerDiscoveryStateVariable.asObservable())
            { setupNetworkState, speakerDiscoveryState in
                return (setupNetworkState: setupNetworkState, speakerDiscoveryState: speakerDiscoveryState)
            }
                .subscribe(weak: self, onNext: SetupConfiguringViewController.updateForConfigurationState)
                .disposed(by: rx_disposeBag)
        }
        
    }
    
    func updateForConfigurationState (_ configurationNetworkState: SetupConfigurationState, andDiscoveryState discoveryState: SpeakerDiscoveryState) {
        
     
        
        if case SetupConfigurationState.configuring(let unconfiguredSpeaker) = configurationNetworkState {
            
            var speakerName : String
            
            if let currentSpeakerName = unconfiguredSpeaker.speakerImage?.name {
                speakerName = currentSpeakerName.replacingOccurrences(of: "Gold Fish", with: "Goldfish")
            } else {
                speakerName = ""
            }
            
            statusLabel.text = Localizations.Setup.Configuring.ConfiguringSpeaker(speakerName)
            makeLoading(visible: true, animated: true)
            updateImageFor(unconfiguredSpeaker: unconfiguredSpeaker)
        }
        if case SetupConfigurationState.configured(let unconfiguredSpeaker, _) = configurationNetworkState {
            
            updateImageFor(unconfiguredSpeaker: unconfiguredSpeaker)
            
            switch discoveryState {
            case .idle: break
            case .discovering(_):
                var speakerName : String
                
                if let currentSpeakerName = unconfiguredSpeaker.speakerImage?.name {
                    speakerName = currentSpeakerName.replacingOccurrences(of: "Gold Fish", with: "Goldfish")
                } else {
                    speakerName = ""
                }
                statusLabel.text = Localizations.Setup.Configuring.SearchingForSpeaker(speakerName)
                makeLoading(visible: true, animated: true)
            case .discovered(let speaker):
                
                var speakerName : String
                
                speakerName = speaker.friendlyName.replacingOccurrences(of: "Gold Fish", with: "Goldfish")
                statusLabel.text = Localizations.Setup.Configuring.FoundSpeaker(speakerName)
                makeLoading(visible: false, animated: true)
            case .discoveryFailed(_):
                
                statusLabel.text = nil
                makeLoading(visible: false, animated: true)
            }
        }
    }
    
    func updateImageFor(unconfiguredSpeaker: UnconfiguredSpeaker) {
        
        if let speakerImage =  ExternalConfig.sharedInstance.speakerImages?.speakerImages?
            .filter({ speakerImage in unconfiguredSpeaker.ssid.hasPrefix(speakerImage.ssid!) ||
                unconfiguredSpeaker.model.hasPrefix(speakerImage.name!) })
            .first {
            
            if let heroImageName = speakerImage.heroImageName {
                heroImage.image = UIImage(named: heroImageName)
            } else {
                heroImage.image = UIImage(named: "hero_placeholder")
            }
            
            }
        
    }
    
    func makeLoading(visible: Bool, animated: Bool) {
        
        let isCurrentlyVisible = spinnerImage.alpha > 0.0
        if visible != isCurrentlyVisible {
            
            UIView.setAlphaOfView(spinnerImage, visible: visible, animated: animated)
        }
    }
    
    @IBAction func onRetryDiscovery(_ sender: Any) {
        
        if let vm = viewModel {
            
            vm.retryDiscovery()
        }
    }
    @IBAction func onConfigureAnother(_ sender: Any) {
        
        if let vm = viewModel {
            
            vm.restartConfiguration()
        }
    }
    @IBAction func onTroubleshooting(_ sender: Any) {
        
        self.delegate?.setupConfiguringDidRequestTroubleshooting(self)
    }
}
