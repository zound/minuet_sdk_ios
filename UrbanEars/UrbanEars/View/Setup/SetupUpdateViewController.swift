//
//  SetupUpdateViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

protocol SetupUpdateViewControllerDelegate: class {
    
    func setupUpdateDidFinish(_ viewController:SetupUpdateViewController)
}

class SetupUpdateViewController: UIViewController {

    var viewModel: SetupUpdateViewModel?
    weak var delegate: SetupUpdateViewControllerDelegate?
    
    @IBOutlet weak var updateTextLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIImageView!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var upToDateIndicator: UIImageView!
    @IBOutlet weak var failIndicator: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateTextLabel.text = Localizations.Setup.Update.Text
        updateTextLabel.font = Fonts.MainContentFont
        
        failIndicator.alpha = 0.0
        upToDateIndicator.alpha = 0.0
        
        let progressFont = Fonts.UrbanEars.ExtraLight(100)
        let bodyFontDescriptor = progressFont.fontDescriptor
        let bodyMonospacedNumbersFontDescriptor = bodyFontDescriptor.addingAttributes(
            [
                UIFontDescriptorFeatureSettingsAttribute: [
                    [
                        UIFontFeatureTypeIdentifierKey: kNumberSpacingType,
                        UIFontFeatureSelectorIdentifierKey: kMonospacedNumbersSelector
                    ]
                ]
            ])
        let bodyMonospacedNumbersFont = UIFont(descriptor: bodyMonospacedNumbersFontDescriptor, size: 100.0)
        percentLabel.font = bodyMonospacedNumbersFont
        
        if let vm = viewModel {
            
            vm.updateStateVariable.asObservable().subscribe(weak: self, onNext: SetupUpdateViewController.updateForUpdateState).disposed(by: rx_disposeBag)
            vm.downloadProgress.asObservable().distinctUntilChanged().map{ String($0) + " %" }.bind(to:percentLabel.rx.text).disposed(by: rx_disposeBag)
        }
    }
    
    
    func updateForUpdateState(_ state: SetupUpdateState) {
        
        updateLoadingFromUpdateState(state)
        updateProgressFromUpdateState(state)
        updateTextFromUpdateState(state)
        updateDone(fromUpdateState: state)
        
        if [.finishedUpdate,.noUpdateAvailable].contains(state) {
            
            runAfterDelay(2.0, block: { [weak self] in
                guard let `self` = self else {return}
                self.delegate?.setupUpdateDidFinish(self)
            })
            
        }
        
        if [.failedUpdate].contains(state) {
            
            runAfterDelay(4.0, block: { [weak self] in
                guard let `self` = self else { return }
                self.delegate?.setupUpdateDidFinish(self)
            })
        }
    }
    
    func updateDone(fromUpdateState state: SetupUpdateState) {
        
        let done = [.finishedUpdate, .noUpdateAvailable].contains(state)
        UIView.setAlphaOfView(upToDateIndicator, visible: done, animated: true)
        let failed = [.failedUpdate].contains(state)
        UIView.setAlphaOfView(failIndicator, visible: failed, animated: true)
    }
    
    func updateLoadingFromUpdateState(_ state: SetupUpdateState) {
        
        let loading = [.checkingForUpdate, .installingUpdate, .restartingSpeaker].contains(state)
        if loading {
            
            UIView.setAlphaOfView(loadingIndicator, visible: true, animated: true)
            loadingIndicator.rotate()
        } else {
            
            UIView.setAlphaOfView(loadingIndicator, visible: false, animated: true)
            loadingIndicator.stopRotation()
        }
    }
    
    func updateProgressFromUpdateState(_ state: SetupUpdateState) {
        
         let updating = (state == .downloadingUpdate)
         UIView.setAlphaOfView(percentLabel, visible: updating, animated: true)
    }
    
    func updateTextFromUpdateState(_ state: SetupUpdateState) {
        
        var newText: String? = nil
        switch state {
        case .idle: newText = nil
        case .checkingForUpdate: newText = Localizations.Setup.Update.State.Checking
        case .downloadingUpdate: newText = Localizations.Setup.Update.State.Downloading
        case .installingUpdate: newText = Localizations.Setup.Update.State.Installing
        case .restartingSpeaker: newText = Localizations.Setup.Update.State.Restarting
        case .finishedUpdate:  newText = Localizations.Setup.Update.State.Finished
        case .noUpdateAvailable:  newText =  Localizations.Setup.Update.State.NoUpdateAvailable
        case .failedUpdate:  newText =  Localizations.Setup.Update.State.UpdateFailed
        }
        
        UIView.transition(with: updateTextLabel, duration: 0.25, options: .beginFromCurrentState, animations: { [weak self] in
            
            guard let `self` = self else {return}
            self.updateTextLabel.text = newText
            }, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let vm = viewModel {
            
            //kick off the update process
            if vm.updateState == SetupUpdateState.idle {
                vm.checkForUpdates()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let vm = viewModel {
            
            vm.checkForUpdates()
        }
    }
}
