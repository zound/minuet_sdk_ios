//
//  SetupFailedConnectViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SetupFailedConnectViewControllerDelegate: class {
    
    func setupFailedConnectDidRequestNext(_ viewController: SetupFailedConnectViewController)
    func setupFailedConnectDidRequestCancel(_ viewController: SetupFailedConnectViewController)
}

class SetupFailedConnectViewController: UIViewController {
    
    weak var delegate: SetupFailedConnectViewControllerDelegate?
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        
        messageLabel.text = Localizations.Setup.FailedConnect.Message
        messageLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Next), for: .normal)
        cancelButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        
        nextButton.layoutIfNeeded()
        cancelButton.layoutIfNeeded()
        
        UIView.setAnimationsEnabled(true)
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
     
        self.delegate?.setupFailedConnectDidRequestNext(self)
    }
    @IBAction func onCancel(_ sender: AnyObject) {
        
        self.delegate?.setupFailedConnectDidRequestCancel(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}
