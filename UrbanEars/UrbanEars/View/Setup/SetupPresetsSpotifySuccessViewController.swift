//
//  SetupPresetsIntroViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol SetupPresetsSpotifySuccessViewControllerDelegate: class {
    
    func setupPresetsSpotifyDidRequestNext(_ viewController:SetupPresetsSpotifySuccessViewController)
}

class SetupPresetsSpotifySuccessViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var accountNameLabel: UILabel!
    
    @IBOutlet weak var sentenceBeginLabel: UILabel!
    @IBOutlet weak var sentenceEndLabel: UILabel!
    
    @IBOutlet weak var continueButton: UIButton!
    var viewModel: SetupPresetsViewModel?
    weak var delegate: SetupPresetsSpotifySuccessViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.SpotifySuccess.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        sentenceBeginLabel.text = Localizations.Setup.SpotifySuccess.YourSpotifyAccount
        sentenceBeginLabel.font = Fonts.MainContentFont
        
        sentenceEndLabel.text = Localizations.Setup.SpotifySuccess.IsLinked
        sentenceEndLabel.font = Fonts.MainContentFont
        
        accountNameLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide._Continue), for: .normal)
        
        continueButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        if let vm = viewModel {
            
            vm.spotifyUsername.asObservable().map{$0 ?? ""}.bind(to:accountNameLabel.rx.text).disposed(by: rx_disposeBag)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    @IBAction func onContinue(_ sender: AnyObject) {
        
        self.delegate?.setupPresetsSpotifyDidRequestNext(self)
    }
    
}
