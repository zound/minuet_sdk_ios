//
//  UrbanEarsUnitTests.swift
//  UrbanEarsUnitTests
//
//  Created by Dolewski Bartosz on 23.01.2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import XCTest
@testable import UrbanEars

class FirmwareStringVersionParserTests: XCTestCase {

    func testFirmware1_3_0() {
        let firmwareParser = FirmwareStringParser()
        
        var testFirmwareString = "ns-mm-FS1234-1234_1.3.0-00"
        var result = firmwareParser.parser(testFirmwareString)
        XCTAssertTrue(result == .equalOrGreaterThan1_3, "Firmware version \(testFirmwareString) should be equal or greater than 1.3")
        
        testFirmwareString = "1.3.0-00"
        result = firmwareParser.parser(testFirmwareString)
        XCTAssertTrue(result == .equalOrGreaterThan1_3, "Firmware version \(testFirmwareString) should be equal or greater than 1.3")
    }
    
    func testFirmware1_2_0() {
        let firmwareParser = FirmwareStringParser()
        
        var testFirmwareString = "ns-mm-FS1234-1234_1.2.0-22"
        var result = firmwareParser.parser(testFirmwareString)
        XCTAssertTrue(result == .lessThan1_3, "Firmware version \(testFirmwareString) should be less than 1.3")
        
         testFirmwareString = "1.2.0-22"
         result = firmwareParser.parser(testFirmwareString)
        XCTAssertTrue(result == .lessThan1_3, "Firmware version \(testFirmwareString) should be less than 1.3")
    }
    
    func testFirmware1_2_0Lotsen() {
        let firmwareParser = FirmwareStringParser()
        
        let testFirmwareString = "ns-mm-FS1234-1234-1234_1.2.3-45_6"
        let result = firmwareParser.parser(testFirmwareString)
        XCTAssertTrue(result == .lessThan1_3, "Firmware version \(testFirmwareString) should be less than 1.3")
    }
    
    func testFirmwareShortString() {
        let firmwareParser = FirmwareStringParser()
        let testFirmwareString = "1.2.0"
        
        let result = firmwareParser.parser(testFirmwareString)
        XCTAssertTrue(result == .lessThan1_3, "Firmware version should be less than 1.3")
    }
    
    func testFirmwareBadString() {
        let firmwareParser = FirmwareStringParser()
        var testFirmwareString = "1.2."
        
        var result = firmwareParser.parser(testFirmwareString)
        XCTAssertNil(result, "Result should be nil for test version string: \(testFirmwareString)")
        
        testFirmwareString = ".A.C"
        result = firmwareParser.parser(testFirmwareString)
        XCTAssertNil(result, "Result should be nil for test version string: \(testFirmwareString)")
        
        testFirmwareString = ".A,C"
        result = firmwareParser.parser(testFirmwareString)
        XCTAssertNil(result, "Result should be nil for test version string: \(testFirmwareString)")
        
        testFirmwareString = ""
        result = firmwareParser.parser(testFirmwareString)
        XCTAssertNil(result, "Result should be nil for test version string: \(testFirmwareString)")
    }
}
